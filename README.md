# Rossum PDF conversion service

## Usage

### To run the app

**Using docker-compose:**

- Run `docker-compose up`

**Locally:**

- Install requirements
- Run redis locally: `docker run -d -p 127.0.0.1:6379:6379 --name=redis-local redis`
- Run postgres locally: `docker run -p 5432:5432 --name=testing-db -e POSTGRES_PASSWORD=postgres -d postgres`
- Run celery locally: `celery -A "rossum.celery.app.celery" -q "worker" -Q "rossum.document_conversions" -E --pool=solo`
- Run the app: `python run.py`

### To run the tests

**Using docker-compose:**

- Run `docker-compose -f docker-compose.test.yaml up`

**Locally:**

- Run postgres locally: `docker run -p 5432:5432 --name=testing-db  -e POSTGRES_PASSWORD=postgres -d postgres`
- Run `pytest tests/`

## API

[OpenAPI UI](http://0.0.0.0:8080/ui) will display all available endpoints along with examples of requests and responses

## Database changes

1. Make changes in models definitions (in `rossum.database.models`)
2. Make sure you have project path added in $PYTHONPATH
3. Run `alembic revision --autogenerate -m 'My changes summary'`.
   It will generate migration script and place it into
   `./alembic/versions/` directory.
4. Run `alembic upgrade head --sql`.
   It will generate SQL with migration commands. Use them and execute carefully
   on production database under `postgres` account.
