from rossum.application.app import create_connexion_app

if __name__ == "__main__":
    app = create_connexion_app()
    app.run(host="0.0.0.0", debug=True, port=8080)
