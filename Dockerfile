ARG BASE_IMAGE=python:3.8-alpine
FROM ${BASE_IMAGE}

USER root

RUN apk add --no-cache --virtual .build-deps \
    build-base \
    g++ \
    libxml2-dev \
    libxslt-dev \
    postgresql-dev \
    gcc \
    python3-dev \
    musl-dev \
    poppler-utils \
    jpeg-dev \
    zlib-dev \
    build-base \
    qpdf-dev


RUN mkdir -p /app

WORKDIR /app
COPY *requirements.txt /app/

RUN echo "Installing requirements" && \
    pip install --upgrade pip==19.3.1 && \
    pip install --no-cache-dir -r requirements.txt -r test-requirements.txt

COPY . /app

USER nobody

EXPOSE 8080
CMD ["python", "run.py"]
LABEL name=rossum-project
