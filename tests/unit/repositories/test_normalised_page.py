import pytest

from rossum.database.models import NormalisedPage
from rossum.database.repositories.normalised_page import NormalisedPageRepository

from ...factories.document import get_file_content


def test_save_document_and_return_id(database, document_factory):
    """Test if NormalisedPage object is actually saved to the database."""
    document = document_factory()
    new_page = NormalisedPage(document_id=document.id, page=1, content=get_file_content(file_name="rossum_logo.jpg"))

    NormalisedPageRepository().save_page(new_page)

    document_pages = database.session.query(NormalisedPage).filter_by(document_id=document.id).all()
    assert len(document_pages) == 1


@pytest.mark.usefixtures("database")
def test_get_page(normalised_page_factory):
    page = normalised_page_factory(page=1)
    result = NormalisedPageRepository().get_page(document_id=page.document_id, page_no=page.page)

    assert result == page
