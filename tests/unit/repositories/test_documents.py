import pytest

from rossum.database.models import Document, DocumentStatus
from rossum.database.repositories.document import DocumentRepository

from ...factories.document import get_file_content


def test_save_document_and_return_id(database):
    """Test if Document object is actually saved to the database."""
    document = Document(status=DocumentStatus.processing, n_pages=3, content=get_file_content())

    document_id = DocumentRepository().save_document_and_return_id(document)

    document = database.session.query(Document).filter_by(id=document_id).one()
    assert document


@pytest.mark.usefixtures("database")
def test_get_document(document_factory):
    doc = document_factory()
    result = DocumentRepository().get_document(document_id=doc.id)

    assert result == doc
