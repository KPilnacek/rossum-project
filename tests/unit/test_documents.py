import io
import os

import pytest

from rossum.database.models import DocumentStatus
from rossum.database.repositories.document import DocumentRepository
from rossum.documents import load_document, load_document_page, upload_document
from rossum.exceptions import DocumentNotFoundError, InvalidPdfError, NormalisedPageNotFoundError


def test_upload_file(mocker):
    """Happy path test.

    Database is mocked since the actual save to db is tested in DocumentRepository tests.
    """
    mocker.patch.object(DocumentRepository, "save_document_and_return_id", return_value=1234)

    script_path = os.path.dirname(__file__)
    test_file_path = os.path.join(script_path, "../test_files/test_cv.pdf")

    with open(test_file_path, "rb") as pdf_file:
        data = pdf_file.read()

    assert upload_document(content=data) == 1234


def test_upload_file_invalid_pdf_error():
    with pytest.raises(InvalidPdfError):
        upload_document(content=b"Definitely not valid pdf string")


@pytest.mark.usefixtures("database")
def test_load_document(document_factory):
    """Happy path test."""
    mocked_document = document_factory(status=DocumentStatus.processing, n_pages=5)
    result = load_document(document_id=mocked_document.id)

    assert result == {
        "status": mocked_document.status.value,
        "n_pages": mocked_document.n_pages,
    }


@pytest.mark.usefixtures("database")
def test_load_document_failed():
    """Test document not found."""
    with pytest.raises(DocumentNotFoundError):
        load_document(document_id=99999)


@pytest.mark.usefixtures("database")
def test_load_document_page(normalised_page_factory):
    page = normalised_page_factory()

    result = load_document_page(document_id=page.document_id, page_no=page.page)

    assert isinstance(result, io.BytesIO)
    assert result.getvalue() == page.content


@pytest.mark.usefixtures("database")
def test_load_document_page_failed():
    with pytest.raises(NormalisedPageNotFoundError):
        load_document_page(document_id=99999, page_no=1)
