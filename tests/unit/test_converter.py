# pylint: disable=protected-access
import io
import os

import PIL.Image
import pytest

from rossum.converter import Converter

script_path = os.path.dirname(__file__)
test_file_path = os.path.join(script_path, "../test_files")


def test_save_image_as_png_bytes():
    img = PIL.Image.open(fp=f"{test_file_path}/rossum_logo.jpg")

    result = Converter(1)._save_image_as_png_bytes(img)
    assert isinstance(result, io.BytesIO)
    assert result.getvalue()[0:4] == b"\x89PNG"


@pytest.mark.parametrize(
    "image_name",
    [
        ("smaller_than_max_size.png"),
        ("wider_than_max_size.jpg"),
        ("taller_than_max_size.jpg"),
        ("bigger_than_max_size.jpg"),
    ],
)
def test_scale_image(image_name: str):
    img = PIL.Image.open(fp=f"{test_file_path}/{image_name}")

    converter = Converter(1)
    result = converter._scale_image(img)

    assert result.size <= converter.MAX_ALLOWED_SIZE
