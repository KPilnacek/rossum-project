import os

import faker
from factory import Faker, LazyAttribute, SelfAttribute, SubFactory
from factory.alchemy import SQLAlchemyModelFactory

from rossum.database.models import Document, DocumentStatus, NormalisedPage, rossum_db


def get_file_content(file_name="test_cv.pdf"):
    """Create empty pdf and return its byte representation."""
    script_path = os.path.dirname(__file__)
    test_file_path = os.path.join(script_path, f"../test_files/{file_name}")

    with open(test_file_path, "rb") as pdf_file:
        data = pdf_file.read()

    return data


class DocumentFactory(SQLAlchemyModelFactory):
    class Meta:
        model = Document
        sqlalchemy_session = rossum_db.session

    class Params:
        file_name = "test_cv.pdf"

    id = Faker("pyint")
    status = DocumentStatus.processing
    n_pages = faker.Faker().random_digit()
    content = LazyAttribute(lambda o: get_file_content(o.file_name))


class NormalisedPageFactory(SQLAlchemyModelFactory):
    class Meta:
        model = NormalisedPage
        sqlalchemy_session = rossum_db.session

    class Params:
        file_name = "rossum_logo.jpg"

    id = Faker("pyint")
    document_id = SelfAttribute("document.id")
    document = SubFactory(DocumentFactory)
    page = faker.Faker().random_digit()
    content = LazyAttribute(lambda o: get_file_content(o.file_name))
