import os

import pytest

from rossum.database.models import Document
from rossum.exceptions import InvalidPdfError

script_path = os.path.dirname(__file__)
test_file_path = os.path.join(script_path, "../../../test_files/test_cv.pdf")


def test_upload_file(client, database, mocker):
    """Happy path test. Correctly save the document to the database and return its id."""
    mocked_celery = mocker.patch("rossum.application.handlers.documents.process_document.delay")

    with open(test_file_path, "rb") as pdf_file:
        data = pdf_file.read()

    headers = {"content-type": "application/pdf"}
    response = client.post("/documents", data=data, headers=headers)

    assert database.session.query(Document).filter_by(id=response.json["id"]).one()
    assert response.status_code == 201
    mocked_celery.assert_called_with(response.json["id"])


def test_upload_file_invalid_pdf(client, mocker):
    """Handle exception during opening the PDF file."""
    mocker.patch(
        "rossum.application.handlers.documents.upload_document", side_effect=InvalidPdfError,
    )

    with open(test_file_path, "rb") as pdf_file:
        data = pdf_file.read()

    headers = {"content-type": "application/pdf"}
    response = client.post("/documents", data=data, headers=headers)

    assert response.status_code == 400
    assert response.json == {"status": "error", "message": "Invalid PDF file"}


def test_upload_file_unexpected_error(client, mocker):
    """Handle unexpected error that can happen during the upload."""
    mocker.patch("rossum.application.handlers.documents.upload_document", side_effect=TypeError)

    with open(test_file_path, "rb") as pdf_file:
        data = pdf_file.read()

    headers = {"content-type": "application/pdf"}
    response = client.post("/documents", data=data, headers=headers)

    assert response.status_code == 500
    assert response.json == {"status": "error", "message": "Unexpected error"}


@pytest.mark.usefixtures("database")
def test_load_document(client, document_factory):
    saved_doc = document_factory()
    response = client.get(f"/documents/{saved_doc.id}")

    assert response.status_code == 200
    assert response.json == {
        "status": saved_doc.status.value,
        "n_pages": saved_doc.n_pages,
    }


@pytest.mark.usefixtures("database")
def test_load_document_not_found(client):
    response = client.get("/documents/1234")

    assert response.status_code == 404
    assert response.json == {"status": "error", "message": "Document not found"}


@pytest.mark.usefixtures("database")
def test_load_document_page(client, normalised_page_factory):
    page = normalised_page_factory()
    response = client.get(f"/documents/{page.document_id}/pages/{page.page}")

    assert response.status_code == 200
    assert response.data == page.content


@pytest.mark.usefixtures("database")
def test_load_document_page_not_found(client):
    response = client.get("/documents/1234/pages/12")

    assert response.status_code == 404
    assert response.json == {"status": "error", "message": "Page not found"}
