from typing import Dict


def get() -> Dict[str, bool]:
    """Return a simple JSON payload for health check purposes."""
    return {"status": "ok", "message": "Pong"}
