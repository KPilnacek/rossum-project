openapi: 3.0.0
info:
  title: Rossum API
  description: Rossum app
  contact:
    name: Peter Strecansky
    email: strecansky.peter@gmail.com
  version: '1.0'
  x-visibility: unlisted
paths:
  /ping:
    get:
      operationId: rossum.application.handlers.ping.get
      tags: [Ping]
      summary: Return a simple JSON payload for health check purposes
      responses:
        '200':
          description: Health check successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Pong'
        '500':
          $ref: '#/components/responses/UnexpectedError'

  /documents:
    post:
      operationId: rossum.application.handlers.documents.post
      tags: [Documents]
      summary: Upload a pdf file for further processing
      requestBody:
        required: true
        content:
          application/pdf:
            schema:
              type: string
              format: binary

      responses:
        '201':
          description: File was successfuly uploaded and forwarded for further processing
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UploadSuccessful'
        '400':
          description: Invalid PDF file
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorMessage'
              example:
                status: "error"
                message: "Invalid PDF file"
        '500':
          $ref: '#/components/responses/UnexpectedError'

  /documents/{document_id}:
    get:
      operationId: rossum.application.handlers.documents.get
      tags: [Documents]
      summary: Return processing status of an uploaded document
      parameters:
        - in: path
          name: document_id
          description: Document ID returned after upload
          schema:
            type: integer
          required: true
          example: 1234
      responses:
        '200':
          description: Loaded document
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UploadedDocument'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/UnexpectedError'

  /documents/{document_id}/pages/{number}:
    get:
      operationId: rossum.application.handlers.documents.get_page
      tags: [Documents]
      summary: Render normalised page of the PDF file
      parameters:
        - in: path
          name: document_id
          description: Document ID returned after upload
          schema:
            type: integer
          required: true
          example: 1234
        - in: path
          name: number
          description: Page number
          schema:
            type: integer
            minimum: 0
            exclusiveMinimum: true
          required: true
          example: 2
      responses:
        '200':
          description: Normalised page in PNG format
          content:
            image/png:
              schema:
                type: string
                format: binary
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/UnexpectedError'

components:
  responses:
    UnexpectedError:
      description: Unexpected Error
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/ErrorMessage'
          example:
            status: "error"
            message: "Unexpected error"

    NotFound:
      description: Not Found
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/ErrorMessage'
          example:
            status: "error"
            message: "Document not found"

  schemas:
    Pong:
      title: Pong
      description: Dummy pong response
      type: object
      properties:
        status:
          type: string
        message:
          type: string
      required:
        - status
        - message
      example:
        status: "ok"
        message: "Pong"

    ErrorMessage:
      type: object
      description: Generic error response
      properties:
        status:
          type: string
        message:
          type: string
      required:
        - status
        - message

    UploadSuccessful:
      title: Upload Successful
      description: Returned data about uploaded file
      type: object
      properties:
        id:
          type: integer
      required:
        - id
      example:
        id: 1234

    UploadedDocument:
      title: Loaded document
      description: TMP
      type: object
      properties:
        status:
          description: State of processing
          type: string
          enum:
            - processing
            - done
        n_pages:
          type: integer
          description: Number of pages
      required:
        - status
        - n_pages
      example:
        status: processing
        n_pages: 3
