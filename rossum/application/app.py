import uuid
from typing import Tuple

import connexion
import structlog
from connexion.resolver import RestyResolver
from flask import Flask, Response, jsonify, request
from kw.structlog_config import configure_structlog
from simplejson.encoder import JSONEncoder

from ..database.models import rossum_db
from ..settings import ROSSUM_DB_URL

logger = structlog.get_logger(__name__)


def create_connexion_app() -> connexion.FlaskApp:
    """Initialize the Connexion app.

    Note: `validate_responses` has to be set to False, otherwise app crashes
    when returning images. For more info, check https://github.com/zalando/connexion/issues/401.
    """
    connexion_app = connexion.FlaskApp(__name__, specification_dir="openapi/", options={"swagger_ui": True})

    connexion_app.add_api(
        specification="schema.yaml",
        validate_responses=False,
        strict_validation=True,
        pass_context_arg_name="request",
        resolver=RestyResolver("rossum.application.handlers"),
    )

    _init_database(connexion_app.app)
    configure_structlog(
        connexion_app.app.debug, json_kwargs={"cls": JSONEncoder}, timestamp_format="iso",
    )
    _register_blueprints(connexion_app.app)

    return connexion_app.app


def _init_database(app: Flask) -> None:
    """Add Flask-SQLAlchemy DB instance to the application."""
    app.config["SQLALCHEMY_DATABASE_URI"] = ROSSUM_DB_URL
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    rossum_db.init_app(app)
    rossum_db.app = app
    rossum_db.create_all()


def _tag_incoming_request() -> None:
    """Tag every incoming request with unique id and path."""
    logger.new(request_id=str(uuid.uuid4()), url=request.path)


def _after_request_handler(response: Response) -> Response:
    """Remove db session after each request."""
    rossum_db.session.remove()
    rossum_db.session.bind.pool.dispose()

    return response


def _unexpected_error_handler(exception: Exception) -> Tuple[Response, int]:
    """Set up custom error handling of uncaught exceptions."""
    logger.exception(exception)
    return jsonify({"status": "error", "message": "Unexpected error"}), 500


def _register_blueprints(app: Flask) -> None:
    app.before_request(_tag_incoming_request)
    app.after_request(_after_request_handler)
    app.register_error_handler(Exception, _unexpected_error_handler)
