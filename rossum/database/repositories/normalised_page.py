from ..models import NormalisedPage
from .base import SQLAlchemyRepository


class NormalisedPageRepository(SQLAlchemyRepository):
    def save_page(self, page: NormalisedPage) -> None:
        """Save new document and return its primary key."""
        self.add(page)
        self.commit()

    def get_page(self, document_id: int, page_no: int) -> NormalisedPage:
        """Load Normalised Page from the database."""
        filters = (
            NormalisedPage.document_id == document_id,
            NormalisedPage.page == page_no,
        )
        return self.session.query(NormalisedPage).filter(*filters).one()
