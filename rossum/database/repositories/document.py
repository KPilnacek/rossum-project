from ..models import Document
from .base import SQLAlchemyRepository


class DocumentRepository(SQLAlchemyRepository):
    def save_document_and_return_id(self, document: Document) -> int:
        """Save new document and return its primary key."""
        self.add(document)
        self.commit()
        return document.id

    def get_document(self, document_id: int) -> Document:
        """Return Document from DB based on its primary key."""
        return self.session.query(Document).filter(Document.id == document_id).one()

    def get_document_for_update(self, document_id: int) -> Document:
        """Return Document from DB selected for update."""
        return self.session.query(Document).filter(Document.id == document_id).with_for_update().one()
