class RossumError(Exception):
    pass


class InvalidPdfError(RossumError):
    pass


class DocumentNotFoundError(RossumError):
    pass


class NormalisedPageNotFoundError(RossumError):
    pass
