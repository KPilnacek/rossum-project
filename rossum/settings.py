import os

ROSSUM_DB_CREDENTIALS = {
    "host": os.getenv("DB_HOST") or "localhost",
    "port": os.getenv("DB_PORT") or 5432,
    "database": os.getenv("DB_DATABASE") or "postgres",
    "user": os.getenv("DB_USER") or "postgres",
    "password": os.getenv("DB_PASSWORD") or "postgres",
}

ROSSUM_DB_URL = "postgresql://{user}:{password}@{host}:{port}/{database}".format(**ROSSUM_DB_CREDENTIALS)

REDIS_BROKER_URL = os.getenv("REDIS_BROKER_URL") or "redis://localhost:6379/0"
