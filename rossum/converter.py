import io
import tempfile

import structlog
from pdf2image import convert_from_bytes
from PIL.ImageOps import autocontrast, fit
from PIL.PpmImagePlugin import PpmImageFile

from rossum.database.models import DocumentStatus, NormalisedPage
from rossum.database.repositories.document import DocumentRepository
from rossum.database.repositories.normalised_page import NormalisedPageRepository


class Converter:
    MAX_ALLOWED_SIZE = (1200, 1600)

    def __init__(self, document_id: int) -> None:
        self.document_id = document_id
        self.log = structlog.get_logger(document_id=self.document_id)
        self.document_repo = DocumentRepository()
        self.page_repo = NormalisedPageRepository()

    def run(self) -> None:
        """Convert PDF file into normalised Images per page."""
        self.log.info("convert_pdf_to_image.start")
        document = self.document_repo.get_document(self.document_id)

        with tempfile.TemporaryDirectory() as path:
            images_from_path = convert_from_bytes(document.content, output_folder=path)
            for page_no, image in enumerate(images_from_path, start=1):
                try:
                    self._process_page(page_number=page_no, converted_image=image)
                except Exception:
                    self.log.exception()

        self._change_document_status_after_processing()

    def _process_page(self, page_number: int, converted_image: PpmImageFile) -> None:
        """Normalize the page and save to the database."""
        self.log.info("_process_page.start", page_number=page_number)

        transformed_page = self._transform_image(page_number, converted_image)
        page_to_save = NormalisedPage(
            document_id=self.document_id, page=page_number, content=transformed_page.getvalue()
        )
        self.page_repo.save_page(page=page_to_save)

    def _transform_image(self, page_number: int, image: PpmImageFile) -> io.BytesIO:
        """Handle image transformations of single page.

        Business rule: page should be rendered to normalised png file.
        """
        self.log.info("_transform_image.start", page_number=page_number)

        normalized_image = self._scale_image(autocontrast(image))
        return self._save_image_as_png_bytes(normalized_image)

    def _scale_image(self, image: PpmImageFile) -> PpmImageFile:
        """Scale image so it fits into pre-defined size.

        Business rule: page should fit into 1200x1600 pixels rectangle.
        """
        if image.size < self.MAX_ALLOWED_SIZE:
            return image

        return fit(image, size=self.MAX_ALLOWED_SIZE)

    @staticmethod
    def _save_image_as_png_bytes(image: PpmImageFile) -> io.BytesIO:
        """Save image in PNG format and return its bytestring representation."""
        image_bytes = io.BytesIO()
        image.save(image_bytes, format="PNG")

        return image_bytes

    def _change_document_status_after_processing(self) -> None:
        """Check if all pages were converted and update status if yes."""
        self.log.info("change_document_status_after_processing")

        document = self.document_repo.get_document_for_update(self.document_id)
        if len(document.pages) == document.n_pages:
            self.log.info("change_document_status_after_processing.done")
            document.status = DocumentStatus.done
            self.document_repo.add(document)
            self.document_repo.commit()
