from io import BytesIO
from typing import Dict, Union

import pikepdf
import structlog
from sqlalchemy.orm.exc import NoResultFound

from rossum.database.models import Document, DocumentStatus
from rossum.database.repositories.document import DocumentRepository
from rossum.database.repositories.normalised_page import NormalisedPageRepository

from .exceptions import DocumentNotFoundError, InvalidPdfError, NormalisedPageNotFoundError

logger = structlog.get_logger()


def upload_document(content: bytes) -> int:
    """Save received PDF file to the database and return its ID."""
    logger.info("upload_document.start")

    try:
        pdf = pikepdf.open(BytesIO(content))
    except pikepdf.PdfError as err:
        logger.exception()
        raise InvalidPdfError from err

    document = Document(status=DocumentStatus.processing, n_pages=len(pdf.pages), content=content)
    document_id = DocumentRepository().save_document_and_return_id(document)

    logger.info("upload_document.document_saved", document_id=document_id)
    return document_id


def load_document(document_id: int) -> Dict[str, Union[str, int]]:
    """Load document from database and return its state and number of pages."""
    logger.info("load_document.start")

    try:
        document = DocumentRepository().get_document(document_id=document_id)
        logger.info("load_document.document_loaded")
        return {"status": document.status.value, "n_pages": document.n_pages}
    except NoResultFound as err:
        logger.exception()
        raise DocumentNotFoundError from err


def load_document_page(document_id: int, page_no: int) -> BytesIO:
    """Load normalised page of the document from the database."""
    logger.info("load_document_page.start", document_id=document_id, page=page_no)

    try:
        page = NormalisedPageRepository().get_page(document_id, page_no)
        logger.info("load_document_page.document_page_loaded", document_id=document_id, page=page_no, page_id=page.id)
        return BytesIO(page.content)
    except NoResultFound as err:
        logger.exception()
        raise NormalisedPageNotFoundError from err
